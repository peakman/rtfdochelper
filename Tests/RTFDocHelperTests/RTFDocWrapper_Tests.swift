//
//  RTFDocWrapper_Tests.swift
//  RTFDocWrapper Tests
//
//  Created by Steve Clarke on 05/07/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

import XCTest
import Utilities
@testable import RTFDocHelper


class RTFDocWrapper_Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testWrapperFromTemplate() {
        // This is an example of a functional test case.
        let template = URL(fileURLWithPath: #file, isDirectory: false).deletingLastPathComponent().deletingLastPathComponent().deletingLastPathComponent().appendingPathComponent("/Sources/RTFDocHelper/Resources", isDirectory: true).appendingPathComponent("default_template.rtf", isDirectory: false)
        let  (attrString , docAttrs ) = RTFDocumentWrapper.richTextFrom(template: template)
        XCTAssertTrue(Regex(pattern: ".*Primrose.*").match(attrString.string))
        XCTAssertNotNil(docAttrs)
        XCTAssertNotNil(docAttrs[NSAttributedString.DocumentAttributeKey.documentType])
        XCTAssertEqual(docAttrs[NSAttributedString.DocumentAttributeKey.documentType] as? String, "NSRTF")
        XCTAssertEqual(docAttrs[NSAttributedString.DocumentAttributeKey.leftMargin] as? NSNumber, 72)
    }
 
    func testWrapperFromString() {
        XCTAssertNoThrow(RTFDocumentWrapper(attrString: NSAttributedString(string: "abc")).fileWrapper)
    }
    
    func testWrite() {
        let template = URL(fileURLWithPath: #file, isDirectory: false).deletingLastPathComponent().deletingLastPathComponent().deletingLastPathComponent().appendingPathComponent("/Sources/RTFDocHelper/Resources", isDirectory: true).appendingPathComponent("default_template.rtf", isDirectory: false)
        let  (attrString , docAttrs ) = RTFDocumentWrapper.richTextFrom(template: template)
        let outDir = FileManager.default.homeDirectoryForCurrentUser
            .appendingPathComponent("DocsNotCloud", isDirectory: true)
            .appendingPathComponent("temp", isDirectory: true)
        let w = RTFDocumentWrapper(attrString: attrString, docAttrs: docAttrs)
        let furl = outDir.appendingPathComponent("test.rtf", isDirectory: false)
        try? FileManager.default.removeItem(at: furl)
        w.writeToDir(outDir, name: "test.rtf")
        XCTAssertTrue(FileManager.default.fileExists(atPath: furl.path))

        
    }
    
    
}
