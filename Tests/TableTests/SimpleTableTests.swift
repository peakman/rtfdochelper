//
//  SimpleTableTests.swift
//  
//
//  Created by Steve Clarke on 16/02/2022.
//

import XCTest

//@testable import RTFDocHelper
import RTFDocHelper

class SimpleTableTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTableCreation() throws {
        let template = URL(fileURLWithPath: #file, isDirectory: false).deletingLastPathComponent().deletingLastPathComponent().deletingLastPathComponent().appendingPathComponent("/Sources/RTFDocHelper/Resources", isDirectory: true).appendingPathComponent("default_template.rtf", isDirectory: false)
        let  (_ , docAttrs ) = RTFDocumentWrapper.richTextFrom(template: template)
        let data : [[String]] = [
            ["able", "baker"],
            ["1", "2"]
        ]
        let tableString = NSMutableAttributedString(data: data, options: [:], custom: nil)
        let outDir = FileManager.default.homeDirectoryForCurrentUser
            .appendingPathComponent("DocsNotCloud", isDirectory: true)
            .appendingPathComponent("temp", isDirectory: true)
        let w = RTFDocumentWrapper(attrString: tableString, docAttrs: docAttrs)
        let furl = outDir.appendingPathComponent("table_string_test.rtf", isDirectory: false)
        try? FileManager.default.removeItem(at: furl)
        w.writeToDir(outDir, name: "table_string_test.rtf")
        XCTAssertTrue(FileManager.default.fileExists(atPath: furl.path))

    }

}
