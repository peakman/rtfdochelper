// swift-tools-version:5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "RTFDocHelper",
    platforms: [
        .macOS(.v14 ),
    ],
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "RTFDocHelper",
            targets: ["RTFDocHelper"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(url: "https://bitbucket.org/peakman/Utilities.git", from: "3.0.6")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "RTFDocHelper",
            dependencies: [],
            exclude: ["Resources/default_template.rtf"]
        ),
        
        .testTarget(
            name: "RTFDocHelperTests",
            dependencies: ["RTFDocHelper", "Utilities"]),
        .testTarget(
            name: "TableTests",
            dependencies: ["RTFDocHelper", "Utilities"]),
    ]
)
