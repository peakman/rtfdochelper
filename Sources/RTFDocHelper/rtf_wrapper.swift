//
//  rtf_wrapper.swift
//  RTFDocWrapper
//
//  Created by Steve Clarke on 04/07/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

import Foundation
import AppKit

public typealias SCDocAttrs = [NSAttributedString.DocumentAttributeKey : Any]

public class RTFDocumentWrapper  {
    public let attributedString : NSMutableAttributedString
    public var docAttrs : SCDocAttrs = [
        NSAttributedString.DocumentAttributeKey.documentType : NSAttributedString.DocumentType.rtf]
    public var fileWrapper : FileWrapper {
        let wrapper : FileWrapper
        do {
            wrapper = try attributedString.fileWrapper(from: NSMakeRange(0, attributedString.length ), documentAttributes: docAttrs)
        } catch {
            fatalError("Failed to create file wrapper. \(error.localizedDescription)")
        }
        return wrapper
    }
    
    public init(attrString: NSAttributedString, docAttrs docAttrsIn: SCDocAttrs = [:] ) {
        if docAttrsIn.count == 0 {
            self.docAttrs = [
                NSAttributedString.DocumentAttributeKey.documentType : NSAttributedString.DocumentType.rtf]
        } else {
            self.docAttrs = docAttrsIn
        }
        self.attributedString = attrString.mutableCopy() as! NSMutableAttributedString
    }

    public init(template: URL ) {
        let  (attrString , attributes) = RTFDocumentWrapper.richTextFrom(template: template)
        self.attributedString = attrString.mutableCopy() as! NSMutableAttributedString
        self.docAttrs = attributes
    }
    
    public class func richTextFrom(template url: URL) -> ( NSAttributedString,  SCDocAttrs)   {
        let docAttrs : SCDocAttrs = [:]
        var docAttrsNS = docAttrs as NSDictionary?
        let readingOptions : [NSAttributedString.DocumentReadingOptionKey : Any] = [
            NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.rtf
        ]
        let attrString : NSAttributedString
        do {
            attrString = try NSAttributedString(url: url, options: readingOptions, documentAttributes: &docAttrsNS )
        } catch {
            fatalError("Unable to create attributedString")
        }
        if docAttrsNS != nil  {
            return (attrString, docAttrsNS as! SCDocAttrs)
        } else {
            return (attrString, docAttrs)
        }
    }

    public func setDocAttribute( _ key : NSAttributedString.DocumentAttributeKey , value : Any) {
        docAttrs[key] = value
    }
    
    public func appendAttributedString( _ attrString : NSAttributedString) {
        attributedString.append(attrString)
    }
    
    public func appendString( _ string : String) {
        attributedString.append(NSAttributedString(string: string))
    }

    public func writeToDir( _ outDir : URL , name: String, docAttrs: SCDocAttrs? = [:]) {
        if docAttrs != nil && docAttrs!.count > 0 {
            self.docAttrs = docAttrs!
        }
        let fpath = outDir.appendingPathComponent(name, isDirectory: false)
        do {
            try self.fileWrapper.write(to: fpath ,
                              options: FileWrapper.WritingOptions.atomic, originalContentsURL: nil)
        } catch {
            fatalError(("Failed to write \(fpath.path). \(error)"))
        }
    }
 

}


