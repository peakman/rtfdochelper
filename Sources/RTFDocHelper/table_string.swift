//
//  table_string.swift
//  
//
//  Created by Steve Clarke on 14/09/2014.
//
//

import Foundation
import AppKit


public class AttributeTable {
    public var attrs : [[[NSAttributedString.Key : AnyObject]]]
    init(attributeTable :  [[[NSAttributedString.Key : AnyObject]]] ) {
        attrs = attributeTable
    }
    
    func textBlockAt(_ row: Int, col: Int) -> NSTextTableBlock {
            return (attrs[row][col][NSAttributedString.Key.paragraphStyle]! as! NSMutableParagraphStyle ).textBlocks[0] as! NSTextTableBlock
    }
    
    public func setAttribute(_ attrName : NSAttributedString.Key!, row: Int, toObject: NSObject) {
        for var attrDict : [NSAttributedString.Key : AnyObject] in attrs[row] {
            attrDict[attrName] = toObject
        }
    }

    public func setAttribute(_ attrName : NSAttributedString.Key!, column: Int, toObject: NSObject) {
        for row in 0..<attrs.count {
            attrs[row][column][attrName] = toObject
        }
    }
    
    public func setColumnAlignment(_ column: Int, alignment: NSTextAlignment) {
        for row in 0..<attrs.count {
            (attrs[row][column][NSAttributedString.Key.paragraphStyle] as! NSMutableParagraphStyle ).alignment = alignment
        }
    }
    
    public func overlineRow(_ row: Int) {
        for column   in 0..<attrs[0].count {
            let textBlock : NSTextTableBlock = textBlockAt(row, col: column)
            textBlock.setBorderColor(NSColor.black, for: .minY )
        }
    }
    
    public func underlineRow(_ row: Int) {
        for column in 0..<attrs[0].count {
            let textBlock : NSTextTableBlock = (attrs[row][column][NSAttributedString.Key.paragraphStyle] as! NSMutableParagraphStyle ).textBlocks[0] as! NSTextTableBlock
            textBlock.setBorderColor(NSColor.black, for: .maxY )
        }
    }

    func sideBorderLeft() {
        for row  in 0..<attrs.count {
            let textBlock : NSTextTableBlock = (attrs[row][0][NSAttributedString.Key.paragraphStyle] as! NSMutableParagraphStyle ).textBlocks[0] as! NSTextTableBlock
            textBlock.setBorderColor(NSColor.black, for: .minX)
        }
    }
    
    func sideBorderRight() {
        for row in 0..<attrs.count {
            let textBlock : NSTextTableBlock = (attrs[row][attrs[0].count - 1][NSAttributedString.Key.paragraphStyle] as! NSMutableParagraphStyle ).textBlocks[0] as! NSTextTableBlock
            textBlock.setBorderColor(NSColor.black, for: .maxX)
        }
    }
    
    func outsideBorder() {
        overlineRow(0)
        underlineRow(attrs.count - 1)
        sideBorderRight()
        sideBorderLeft()
    }
    
}

public enum TableStringOptions  {
    case drawBorders, fontSize
}

extension NSMutableAttributedString {
    public convenience init(
        data: [[String]],
        options: [TableStringOptions: AnyObject]? = nil,
        custom: ((AttributeTable) -> [[[NSAttributedString.Key : AnyObject]]])? = nil ) {
            self.init(string:"")
            var drawBorders = true
            var fontSize = CGFloat(10.0)
            if let opts = options {
                if let drawOpt : AnyObject = opts[.drawBorders] {
                    drawBorders = drawOpt as! Bool
                }
                if let fontSzOpt : AnyObject = opts[.fontSize] {
                    fontSize = fontSzOpt as! CGFloat
                }
            }
            let text_table = NSTextTable()
            text_table.numberOfColumns = data[0].count
            text_table.collapsesBorders = false
            let para_style = NSMutableParagraphStyle()
            para_style.headIndent = CGFloat(0.0)
            para_style.tailIndent = CGFloat(0.0)
            var attr_table : [[[NSAttributedString.Key : AnyObject]]] = []
            var tb : NSTextTableBlock
            var cell_style : NSMutableParagraphStyle
            var cell_attrs : [NSAttributedString.Key : AnyObject]
            var columnWidths : [CGFloat]
            for row in (0..<data.count) {
                attr_table.append([])
                for col in (0..<data[0].count) {
                    tb = NSTextTableBlock(table: text_table, startingRow: row, rowSpan: 1, startingColumn: col, columnSpan: 1)
                    if drawBorders {
                        tb.setWidth(1.0 , type: .absoluteValueType, for: .border)
                    }
                    tb.setWidth(1.0 , type: .absoluteValueType, for: .padding)
                    tb.setWidth(0.0 , type: .absoluteValueType, for: .margin)
                    cell_style = NSMutableParagraphStyle()
                    cell_style.setParagraphStyle(para_style)
                    cell_style.textBlocks = [tb]
                    cell_attrs = [ NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : NSFont(name: "Helvetica", size: fontSize)!]
                    cell_attrs[NSAttributedString.Key.paragraphStyle]=cell_style
                    attr_table[row].append(cell_attrs)
                }
            }
            var newAttrTable: AttributeTable
            if let ct = custom {
                newAttrTable = AttributeTable(attributeTable: ct(AttributeTable(attributeTable: attr_table)))
            } else {
                newAttrTable = AttributeTable(attributeTable: attr_table)
            }
            columnWidths = setColumnWidths(data, attrTable: newAttrTable)
            for row in (0..<data.count) {
                var ttb: NSTextTableBlock
                for col in (0..<data[0].count) {
                    ttb = newAttrTable.textBlockAt(row, col: col)
                    ttb.setContentWidth(columnWidths[col] ,type:  .absoluteValueType)
                    self.append(NSAttributedString(string: "\(data[row][col])\n" , attributes: newAttrTable.attrs[row][col] ))
                }
            }
    }
    
    func setColumnWidths(_ data: [[String]], attrTable: AttributeTable) -> [CGFloat] {
        var colWidths = [CGFloat]()
        var max : CGFloat
        var width : CGFloat
        for col in (0..<data[0].count) {
            max = CGFloat(0.0)
            for row in (0..<data.count) {
                width = widthOf(NSAttributedString(string:  data[row][col] , attributes:  attrTable.attrs[row][col] ))
                max = (max > width ) ? max :   width
            }
            colWidths.append(max)
        }
        return colWidths
    }
    
    func widthOf(_ attrString: NSAttributedString) -> CGFloat {
        //var attrString =
        return attrString.boundingRect( with: NSMakeSize(CGFloat(100),CGFloat(100)), options: []).size.width + CGFloat(10)
    }
}
